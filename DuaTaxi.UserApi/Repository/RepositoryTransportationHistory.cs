﻿using DuaTaxi.Common.Mongo;
using DuaTaxi.UserApi.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.UserApi.Repository
{
    public class RepositoryTransportationHistory : IRepositoryTransportationHistory
    {

        IMongoRepository<UserTransportationHistory> _repository;
        public RepositoryTransportationHistory(IMongoRepository<UserTransportationHistory> repository)
        {
            _repository = repository;
        }
        public async Task AddAsync(UserTransportationHistory history)
            => await _repository.AddAsync(history);

        public async Task DeleteAsync(UserTransportationHistory history)
            => await _repository.DeleteAsync(history.Id);

        public async Task<UserTransportationHistory> GetAsync(string Id)
            => await _repository.GetAsync(Id);

        public async Task<IEnumerable<UserTransportationHistory>> GetDriverByIdAsync(string Id)
            => await _repository.FindAsync(c => c.DriverId == Id);

        public async Task UpdateAsync(UserTransportationHistory history)
            => await _repository.UpdateAsync(history);
    }
}
