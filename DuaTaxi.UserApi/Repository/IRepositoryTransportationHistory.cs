﻿using DuaTaxi.UserApi.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.UserApi.Repository
{
    public interface IRepositoryTransportationHistory
    {
        Task AddAsync(UserTransportationHistory history);
        Task UpdateAsync(UserTransportationHistory history);

        Task DeleteAsync(UserTransportationHistory history);

        Task<UserTransportationHistory> GetAsync(string Id);

        Task<IEnumerable<UserTransportationHistory>> GetDriverByIdAsync(string Id);

    }
}
