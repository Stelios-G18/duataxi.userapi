﻿using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.UserApi.Entities.Models
{
    public class UserTransportationHistory : BaseEntity
    {
        public string From { get; set; }
        public string To { get; set; }
        public string DriverName { get; set; }
        public string DriverId { get; set; }
        public string DriverPositionStatus { get; set; }
        public List<Marker> Marker { get; set; }

        public UserTransportationHistory() : base()
        {

        }
    }
}
