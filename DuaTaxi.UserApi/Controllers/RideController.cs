﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.UserApi.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DuaTaxi.UserApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RideController : ControllerBase
    {


        [HttpPost]
        [Route("Create/")]
        public async Task<SrvResp<UserTransportationHistory>> Create([FromBody] UserTransportationHistory payment)
        {
            return new SrvResp<UserTransportationHistory>();
        }


        [HttpPost]
        [Route("Update/")]
        public async Task<SrvResp<UserTransportationHistory>> Update([FromBody] UserTransportationHistory payment)
        {
            return new SrvResp<UserTransportationHistory>();
        }
    }
}